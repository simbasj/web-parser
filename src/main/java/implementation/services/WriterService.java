package implementation.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import core.entities.TechnologyStack;
import core.services.IWriterService;

import java.io.IOException;
import java.util.HashSet;

public class WriterService implements IWriterService {
    public void Write(HashSet<TechnologyStack> technologyStacks) {
        try {
            ObjectMapper mapper = new ObjectMapper();
            String json = mapper.writeValueAsString(technologyStacks);
            Object jsonObject = mapper.readValue(json, Object.class);
            String prettyJson = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(jsonObject);
            System.out.println(prettyJson);

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
