package core.entities;

import java.util.HashSet;

public class TechnologyStack {
    private String  _area;
    private HashSet<String> _technologies;

    public TechnologyStack()
    {
        _technologies = new HashSet<>();
    }

    public String getArea() { return _area; }
    public void setArea(String area) { _area = area; }

    public HashSet<String> getTechnologies() { return _technologies; }
    public void setTechnologies(String technology) {
        _technologies.add(technology);
    }

}
