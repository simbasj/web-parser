package core.services;

import core.entities.TechnologyStack;

import java.util.HashSet;

public interface IWriterService {
    public void Write(HashSet<TechnologyStack> technologyStacks);
}