package ui.desktop;

import core.services.IParserService;
import core.services.IWriterService;
import implementation.services.ParserService;
import implementation.services.WriterService;

public class ConsoleApplication {
    public static void main(String[] args) {
        try {
            String url = "https://github.com/egis/handbook/blob/master/Tech-Stack.md";
            IParserService parserService = new ParserService();
            IWriterService writerService = new WriterService();
            writerService.Write(parserService.Parse(url));
        } catch (Exception e) {
            System.err.println(e.getMessage());
        }
    }
}
