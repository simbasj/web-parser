package implementation.services;

import core.entities.TechnologyStack;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.HashSet;

import static org.junit.Assert.*;

public class ParserServiceTests {

    ParserService _parserService;

    @Before
    public void setUp() throws Exception {
        _parserService = new ParserService();
    }

    @Test
    public void Should_Return_An_Empty_List() {
        String url = "";
        HashSet<TechnologyStack> technologyStacks =_parserService.Parse(url);
        Assert.assertTrue(technologyStacks.isEmpty());
    }
}