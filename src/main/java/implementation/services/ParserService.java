package implementation.services;

import core.entities.TechnologyStack;
import core.services.IParserService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.HashSet;

public class ParserService implements IParserService {

    private String[] _acceptableTechnologyAreas;

    public ParserService() {
        _acceptableTechnologyAreas = "Programming,Build, Infrastructure".split("\\s*,\\s*");
    }

    public HashSet<TechnologyStack> Parse(String url) {
        HashSet<TechnologyStack> technologyStacks = new HashSet<>();
        try {
            if (url == null || url.isEmpty()) {
                return technologyStacks;
            }
            Document document = Jsoup.connect(url).get();
            Elements headings = document.select("#readme > article > h2");
            for (Element heading : headings) {
                String technologyArea = heading.text();
                boolean isAcceptable = IsMatching(technologyArea, _acceptableTechnologyAreas);
                if (!isAcceptable) continue;
                TechnologyStack technologyStack = new TechnologyStack();
                technologyStack.setArea(heading.text());
                for (Element row : heading.nextElementSibling().select("table > tbody > tr")) {
                    String technology = row.select("td").get(0).text();
                    if (!technologyStack.getTechnologies().contains(technology)) {
                        technologyStack.setTechnologies(technology);
                    }
                }
                technologyStacks.add(technologyStack);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return technologyStacks;
    }

    private static boolean IsMatching(String input, String[] matches) {
        boolean found = false;
        for (String s : matches) {
            if (input.contains(s)) {
                found = true;
                break;
            }
        }
        return found;
    }
}
