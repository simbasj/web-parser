package core.services;

import core.entities.TechnologyStack;

import java.io.IOException;
import java.util.HashSet;

public interface IParserService {
    public HashSet<TechnologyStack> Parse(String url) throws IOException;
}